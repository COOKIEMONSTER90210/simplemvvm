﻿using System;

namespace Nord.Common.Reactive
{
    public class EventProvider : IEventProvider
    {
        private static IEventProvider _emptyEventProvider;

        public event Action OnChanged;

        public static IEventProvider Empty => _emptyEventProvider ??= new EventProvider();

        public class Mutable : EventProvider
        {
            public void CallChanged() => OnChanged?.Invoke();
        }
    }
}