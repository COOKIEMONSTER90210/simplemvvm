﻿using System;

namespace Nord.Common.Reactive.Generic
{
    public class EventProvider<T> : IEventProvider<T>
    {
        public event Action OnChanged;

        private T _value;
        private static IEventProvider<T> _empty;

        public EventProvider()
        {
            _value = default;
        }

        public static IEventProvider<T> Empty => _empty ??= new EventProvider<T>();
        
        public void Dispose()
        {
            OnChanged = null;
        }

        public void Call(T value)
        {
            _value = value;
            OnChanged?.Invoke();
        }

        T IEventProvider<T>.Value => _value;
    }
}