﻿namespace Nord.Common.Reactive.Generic
{
    /// <summary>
    /// provides a generic event
    /// </summary>
    public interface IEventProvider<out T> : IEventProvider
    {
        T Value { get; }
    }
}