﻿using System.Collections.Generic;

namespace Nord.Common.Reactive.Generic
{
    /// <summary>
    /// provides an IReadOnlyList
    /// </summary>
    public interface IReactiveListProperty<T> : IReactiveProperty<IReadOnlyList<T>>
    {
    }
}