﻿namespace Nord.Common.Reactive.Generic
{
    /// <summary>
    /// provides a generic event
    /// </summary>
    public interface IReactiveProperty<out T> : IEventProvider
    {
        T Value { get; }
    }
}