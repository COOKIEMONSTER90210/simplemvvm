﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;

namespace Nord.Common.Reactive.Generic
{
    public class ReactiveListProperty<TItem> : ReactiveListProperty<TItem, TItem>
    {
        public ReactiveListProperty(IReadOnlyList<TItem> value) : base(value)
        {
        }
    }

    public class ReactiveListProperty<TOutputItem, TMutableItem> : IReactiveListProperty<TOutputItem> where TMutableItem : TOutputItem
    {
        private IReadOnlyList<TMutableItem> _value;
        private IReadOnlyList<TOutputItem> _outputValue;

        public ReactiveListProperty(IReadOnlyList<TMutableItem> value)
        {
            _value = value;
        }

        public event Action OnChanged;

        public IReadOnlyList<TMutableItem> Value
        {
            get => _value;
            set
            {
                if (_value != null && _value.Equals(value) || _value == null && value == null)
                {
                    return;
                }

                SetValue(value);
            }
        }

        IReadOnlyList<TOutputItem> IReactiveProperty<IReadOnlyList<TOutputItem>>.Value
        {
            get
            {
                if (_outputValue == null && _value != null)
                {
                    _outputValue = _value.Cast<TOutputItem>().ToList().AsReadOnly();
                }

                return _outputValue;
            }
        }

        public void SetWithForceUpdate(IReadOnlyList<TMutableItem> value)
        {
            SetValue(value);
        }

        private void SetValue(IReadOnlyList<TMutableItem> value)
        {
            _value = value;
            _outputValue = _value as ReadOnlyCollection<TOutputItem>;
            OnChanged?.Invoke();
        }
    }
}