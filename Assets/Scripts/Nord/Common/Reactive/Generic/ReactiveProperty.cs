using System;
using System.Collections.Generic;

namespace Nord.Common.Reactive.Generic
{
    public class ReactiveProperty<T> : IReactiveProperty<T>
    {
        public event Action OnChanged;

        private static readonly IEqualityComparer<T> EqualityComparer = EqualityComparer<T>.Default;

        private T _value;

        public ReactiveProperty(T value = default(T))
        {
            _value = value;
        }

        public void Dispose()
        {
            OnChanged = null;
        }

        public T Value
        {
            get => _value;
            set
            {
                if (EqualityComparer.Equals(value, _value))
                {
                    return;
                }

                _value = value;
                OnChanged?.Invoke();
            }
        }

        public void SetWithForceUpdate(T value)
        {
            _value = value;
            OnChanged?.Invoke();
        }

        T IReactiveProperty<T>.Value => _value;
    }

    public class ReactiveProperty<TOutputValue, TMutableValue> : IReactiveProperty<TOutputValue>
        where TMutableValue : TOutputValue
    {
        public event Action OnChanged;

        private static readonly IEqualityComparer<TMutableValue> EqualityComparer =
            EqualityComparer<TMutableValue>.Default;

        private TMutableValue _value;

        public ReactiveProperty(TMutableValue value = default(TMutableValue))
        {
            _value = value;
        }

        public void Dispose()
        {
            OnChanged = null;
        }

        public TMutableValue Value
        {
            get => _value;
            set
            {
                if (EqualityComparer.Equals(value, _value))
                {
                    return;
                }

                _value = value;
                OnChanged?.Invoke();
            }
        }

        public void SetWithForceUpdate(TMutableValue value)
        {
            _value = value;
            OnChanged?.Invoke();
        }

        TOutputValue IReactiveProperty<TOutputValue>.Value => _value;
    }
}