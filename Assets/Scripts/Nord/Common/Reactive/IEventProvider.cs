using System;

namespace Nord.Common.Reactive
{
    /// <summary>
    /// provides an event
    /// </summary>
    public interface IEventProvider
    {
        event Action OnChanged;
    }
}