using System;

namespace Samples.Bubbles.Logic
{
    public class Bubble : IBubble
    {
        public event Action PositionChanged;
        
        private readonly int _id; // unique, see usage
        private float _size; // 1..inf
        private float _x; // 0..1
        private float _y; // 0..1

        public Bubble(int id)
        {
            _id = id;
        }

        public void Reuse(float size, float x)
        {
            _size = size;
            _x = x;
            _y = 0;
        }

        public void SetY(float y)
        {
            _y = y;
            PositionChanged?.Invoke();
        }
        
        public int GetId() => _id;
        public float GetSize() => _size;

        public float GetX() => _x;

        public float GetY() => _y;
    }
}