using Random = UnityEngine.Random;

namespace Samples.Bubbles.Logic
{
    public class BubblesFactory
    {
        private readonly SimplePool<Bubble> _pool;
        private int _maxBubbleId = 0;

        public BubblesFactory()
        {
            _pool = new SimplePool<Bubble>(() => new Bubble(++_maxBubbleId));
        }

        public Bubble Get()
        {
            var result = _pool.Get();
            result.Reuse(Random.Range(1.0f, 2.0f), Random.Range(0.0f, 1.0f));
            return result;
        }

        public void Free(Bubble bubble) => _pool.Free(bubble);
    }
}