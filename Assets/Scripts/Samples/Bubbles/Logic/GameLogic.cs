using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace Samples.Bubbles.Logic
{
    /// <summary>
    /// Hard code. Just for an example
    /// </summary>
    public class GameLogic
    {
        private readonly BubblesFactory _factory = new BubblesFactory();
        private readonly List<Bubble> _bubbles = new List<Bubble>();
        private float _secondsFromStart = 0.0f;
        private float _secondsFromLastSpawn = 0.0f;
        private int _score;
        
        private bool _isGameFinished;

        private const float SpawnBubbleInterval = 1.0f;
        private const int MaxBubbles = 25;

        public event Action Tick;
        public event Action<IBubble> BubbleAdded;
        public event Action<IBubble> BubbleRemoved;
        public event Action<IBubble> BubbleDied;

        public void StartNewGame()
        {
            foreach (var bubble in _bubbles)
            {
                _factory.Free(bubble);
            }
            _bubbles.Clear();
            _secondsFromStart = 0;
            _secondsFromLastSpawn = 0;
            _score = 0;
            _isGameFinished = false;
        }
        
        public void Update(float dt)
        {
            // we have already finished
            if (_isGameFinished)
            {
                return;
            }
            
            _secondsFromStart += dt;
            _secondsFromLastSpawn += dt;

            // spawn bubbles
            if (_secondsFromLastSpawn >= SpawnBubbleInterval && _bubbles.Count < MaxBubbles)
            {
                var bubble = _factory.Get();
                _bubbles.Add(bubble);
                _secondsFromLastSpawn = 0;
                BubbleAdded?.Invoke(bubble);
            }

            // update bubbles and check end game conditions
            for (int i = 0; i < _bubbles.Count; i++)
            {
                var bubble = _bubbles[i];
                bubble.SetY(bubble.GetY() + bubble.GetSize() * dt * GetVelocity());

                if (bubble.GetY() > 1.0f)
                {
                    _isGameFinished = true;
                    BubbleDied?.Invoke(bubble);
                }
            }
            
            Tick?.Invoke();
        }

        private float GetVelocity() => _secondsFromStart * 0.025f;
        
        public int GetVelocityForUI() => (int) (GetVelocity() * 100);

        public int GetScore() => _score;

        public void RemoveBubble(IBubble bubble)
        {
            if (_isGameFinished)
            {
                return;
            }
            
            if (bubble == null)
            {
                Debug.LogError("GameLogic.RemoveBobble: bubble == null");
                return;
            }

            var candidate = _bubbles.FirstOrDefault(e => e.GetId() == bubble.GetId());

            if (candidate == null)
            {
                Debug.LogError("GameLogic.RemoveBobble: candidate == null");
                return;
            }

            _bubbles.Remove(candidate);
            _factory.Free(candidate);
            _score++;
            BubbleRemoved?.Invoke(bubble);
        }
    }
}