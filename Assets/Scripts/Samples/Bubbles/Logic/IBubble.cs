using System;

namespace Samples.Bubbles.Logic
{
    public interface IBubble
    {
        event Action PositionChanged;
        
        int GetId();
        float GetSize();
        float GetX();
        float GetY();
    }
}