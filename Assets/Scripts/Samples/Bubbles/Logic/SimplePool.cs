using System;
using System.Collections.Generic;

namespace Samples.Bubbles.Logic
{
    public class SimplePool<T> where T : class
    {
        private readonly Func<T> _factoryMethod;
        private readonly Stack<T> _freeObjects;

        public SimplePool(Func<T> factoryMethod)
        {
            _factoryMethod = factoryMethod;
            _freeObjects = new Stack<T>();
        }

        public T Get()
        {
            return _freeObjects.Count == 0 ? _factoryMethod() : _freeObjects.Pop();
        }

        public void Free(T obj)
        {
            _freeObjects.Push(obj);
        }
    }
}