﻿using Nord.Common.Unity.Reactive;
using Samples.Bubbles.Logic;
using Samples.Bubbles.Router;
using Samples.Bubbles.View;
using UnityEngine;

namespace Samples.Bubbles
{
    /// <summary>
    /// Composition root
    /// </summary>
    public class Main : MonoBehaviour
    {
        [SerializeField] private ViewFactory viewFactory;
        [SerializeField] private RectTransform root;
        private GameLogic _logic;
        private MainRouter _router; 
        
        protected void Awake()
        {
            _logic = new GameLogic();
            
            _router = new MainRouter(_logic, viewFactory, root);
            _router.DisposeOnDestroy(gameObject);
            _router.StartNewGame();
        }

        protected void Update()
        {
            _logic.Update(Time.deltaTime);
        }
    }
}