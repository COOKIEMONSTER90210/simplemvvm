using System;

namespace Samples.Bubbles.Router
{
    public interface IMainRouter : IDisposable
    {
        void StartNewGame();
    }
}