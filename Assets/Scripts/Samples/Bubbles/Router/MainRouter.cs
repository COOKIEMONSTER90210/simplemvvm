using System;
using Samples.Bubbles.Logic;
using Samples.Bubbles.View;
using Samples.Bubbles.ViewModel;
using UnityEngine;
using Object = UnityEngine.Object;

namespace Samples.Bubbles.Router
{
    public class MainRouter : IMainRouter
    {
        private readonly GameLogic _logic;
        private readonly ViewFactory _factory;
        private readonly RectTransform _root;

        public MainRouter(GameLogic logic, ViewFactory factory, RectTransform root)
        {
            _logic = logic;
            _factory = factory;
            _root = root;

            _logic.BubbleDied += HandleBubbleDied;
        }

        void IDisposable.Dispose()
        {
            _logic.BubbleDied -= HandleBubbleDied;
        }

        private void HandleBubbleDied(IBubble bubble)
        {
            _factory.ShowResults(new ResultsViewModel(_logic, this), _root);
        }

        private void Clear()
        {
            foreach (Transform child in _root.transform) {
                Object.Destroy(child.gameObject);
            }
        }
        
        public void StartNewGame()
        {
            Clear();
            _logic.StartNewGame();
            _factory.ShowMainField(new MainFieldViewModel(_logic), _root);
            _factory.ShowMainHud(new MainHUDViewModel(_logic), _root);
        }
    }
}