using Nord.Common.Unity.Reactive;
using Samples.Bubbles.ViewModel;
using UnityEngine;

namespace Samples.Bubbles.View
{
    public class BubbleView : MonoBehaviour
    {
        private IBubbleViewModel _viewModel;

        private Vector2 _initialSize;
        
        protected void Awake()
        {
            _initialSize = ((RectTransform) transform).rect.size;
        }
        
        public void Init(IBubbleViewModel viewModel)
        {
            gameObject.UpdateDisposableViewModel(ref _viewModel, viewModel);
            
            gameObject.Subscribe(_viewModel.NormalizedPosition, NormalizedPositionChanged);
            
            // reuse initial size (view is from pool)
            var tr = (RectTransform) transform;
            tr.sizeDelta = _initialSize * _viewModel.Size;
        }

        private void NormalizedPositionChanged(Vector2 p)
        {
            var tr = (RectTransform) transform;
            var parentRect = ((RectTransform)tr.parent).rect;
            tr.anchoredPosition = tr.rect.center + parentRect.size * p - parentRect.size * 0.5f;
        }

        public void OnClicked()
        {
            _viewModel.HandleClick();
        }
    }
}