using Nord.Common.Unity.Reactive;
using Samples.Bubbles.ViewModel;
using UnityEngine;
using UnityEngine.UI;

namespace Samples.Bubbles.View
{
    public class LabelView : MonoBehaviour
    {
        [SerializeField] private Text text;

        private ILabelViewModel _viewModel;

        public void Init(ILabelViewModel viewModel)
        {
            gameObject.UpdateSimpleViewModel(ref _viewModel, viewModel);
            
            gameObject.Subscribe(viewModel.Value, value => text.text = value);
        }
    }
}
