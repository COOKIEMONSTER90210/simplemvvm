using Nord.Common.Unity.Reactive;
using Samples.Bubbles.ViewModel;
using UnityEngine;

namespace Samples.Bubbles.View
{
    public class MainFieldView : MonoBehaviour
    {
        [SerializeField] private RectTransform root;
        [SerializeField] private BubbleView bubblePrefab;

        private IMainFieldViewModel _viewModel;
        private ViewPool<BubbleView, IBubbleViewModel> _pool;

        protected void Awake()
        {
            _pool = new ViewPool<BubbleView, IBubbleViewModel>(
                factoryMethod: () => Instantiate(bubblePrefab, root, false),
                initAction: (view, viewModel) => view.Init(viewModel));
        }
        
        public void Init(IMainFieldViewModel viewModel)
        {
            gameObject.UpdateDisposableViewModel(ref _viewModel, viewModel);
            
            gameObject.Subscribe(_viewModel.BubbleAdded, AddBubble);
            gameObject.Subscribe(_viewModel.BubbleRemoved, RemoveBubble);
        }

        private void AddBubble(IBubbleViewModel viewModel)
        {
            _pool.Get(viewModel);
        }

        private void RemoveBubble(IBubbleViewModel viewModel)
        {
            _pool.Free(viewModel);
        }
    }
}