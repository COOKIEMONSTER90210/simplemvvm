using Nord.Common.Unity.Reactive;
using Samples.Bubbles.ViewModel;
using UnityEngine;

namespace Samples.Bubbles.View
{
    public class MainHUD : MonoBehaviour
    {
        [SerializeField] private LabelView scoreLabelView;
        [SerializeField] private LabelView velocityLabelView;

        private IMainHUDViewModel _viewModel;
        
        public void Init(IMainHUDViewModel viewModel)
        {
            gameObject.UpdateDisposableViewModel(ref _viewModel, viewModel);
            
            scoreLabelView.Init(viewModel.Score);
            velocityLabelView.Init(viewModel.Velocity);
        }
    }
}
