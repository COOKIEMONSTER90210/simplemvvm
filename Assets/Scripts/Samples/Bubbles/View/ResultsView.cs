﻿using Nord.Common.Unity.Reactive;
using Samples.Bubbles.ViewModel;
using UnityEngine;
using UnityEngine.UI;

namespace Samples.Bubbles.View
{
    public class ResultsView : MonoBehaviour
    {
        [SerializeField] private Text score;
        [SerializeField] private Text velocity;

        private IResultsViewModel _viewModel;
        
        public void Init(IResultsViewModel viewModel)
        {
            gameObject.UpdateSimpleViewModel(ref _viewModel, viewModel);

            score.text = viewModel.ScoreText;
            velocity.text = viewModel.VelocityText;
        }

        public void OnNewGameClicked()
        {
            _viewModel.StartNewGame();
        }
    }
}