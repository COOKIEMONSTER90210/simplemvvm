﻿using Samples.Bubbles.ViewModel;
using UnityEngine;

namespace Samples.Bubbles.View
{
    public class ViewFactory : MonoBehaviour
    {
        [SerializeField] private MainHUD mainHudPrefab;
        [SerializeField] private MainFieldView mainFieldViewPrefab;
        [SerializeField] private ResultsView resultsViewPrefab;

        public void ShowMainHud(IMainHUDViewModel viewModel, RectTransform parent)
        {
            var view = Instantiate(mainHudPrefab, parent, false);
            view.Init(viewModel);
        }

        public void ShowMainField(IMainFieldViewModel viewModel, RectTransform parent)
        {
            var view = Instantiate(mainFieldViewPrefab, parent, false);
            view.Init(viewModel);
        }

        public void ShowResults(IResultsViewModel viewModel, RectTransform parent)
        {
            var view = Instantiate(resultsViewPrefab, parent, false);
            view.Init(viewModel);
        }
    }
}