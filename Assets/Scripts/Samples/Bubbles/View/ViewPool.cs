using System;
using System.Collections.Generic;
using UnityEngine;

namespace Samples.Bubbles.View
{
    public class ViewPool<TView, TViewModel> where TView : MonoBehaviour
    {
        private readonly Func<TView> _factoryMethod;
        private readonly Action<TView, TViewModel> _initAction;
        private readonly Stack<TView> _freeViews;
        private readonly Dictionary<TViewModel, TView> _usedViews = new Dictionary<TViewModel, TView>();

        public ViewPool(Func<TView> factoryMethod, Action<TView, TViewModel> initAction)
        {
            _factoryMethod = factoryMethod;
            _initAction = initAction;
            _freeViews = new Stack<TView>();
        }
        
        public TView Get(TViewModel viewModel)
        {
            TView view;
            if (_freeViews.Count == 0)
            {
                view = _factoryMethod();
            }
            else
            {
                view = _freeViews.Pop();
                view.gameObject.SetActive(true);
            }

            _initAction(view, viewModel);
            _usedViews[viewModel] = view;
            return view;
        }

        public void Free(TViewModel viewModel)
        {
            if (_usedViews.TryGetValue(viewModel, out var view))
            {
                _freeViews.Push(view);
                view.gameObject.SetActive(false);
            }
        }
    }
}