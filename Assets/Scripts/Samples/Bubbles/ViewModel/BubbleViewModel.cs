using System;
using Nord.Common.Reactive.Generic;
using Samples.Bubbles.Logic;
using UnityEngine;

namespace Samples.Bubbles.ViewModel
{
    public class BubbleViewModel : IBubbleViewModel
    {
        private readonly GameLogic _logic;
        private readonly IBubble _bubble;
        private readonly ReactiveProperty<Vector2> _normalizedPosition = new ReactiveProperty<Vector2>();

        public BubbleViewModel(GameLogic logic, IBubble bubble)
        {
            _logic = logic;
            _bubble = bubble;
            _bubble.PositionChanged += PositionChanged;
            PositionChanged();
        }

        void IDisposable.Dispose()
        {
            _bubble.PositionChanged -= PositionChanged;
        }

        private void PositionChanged()
        {
            _normalizedPosition.Value = new Vector2(_bubble.GetX(), 1.0f - _bubble.GetY());
        }

        public int GetId() => _bubble.GetId();

        IReactiveProperty<Vector2> IBubbleViewModel.NormalizedPosition => _normalizedPosition;

        float IBubbleViewModel.Size => _bubble.GetSize();

        void IBubbleViewModel.HandleClick() => _logic.RemoveBubble(_bubble);
    }
}