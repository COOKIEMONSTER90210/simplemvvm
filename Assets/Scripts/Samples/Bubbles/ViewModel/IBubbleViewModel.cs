﻿using System;
using Nord.Common.Reactive.Generic;
using UnityEngine;

namespace Samples.Bubbles.ViewModel
{
    public interface IBubbleViewModel : IDisposable
    {
        IReactiveProperty<Vector2> NormalizedPosition { get; }

        float Size { get; }

        void HandleClick();
    }
}