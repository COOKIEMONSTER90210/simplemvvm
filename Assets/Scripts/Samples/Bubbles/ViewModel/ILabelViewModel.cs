using Nord.Common.Reactive.Generic;

namespace Samples.Bubbles.ViewModel
{
    public interface ILabelViewModel
    {
        IReactiveProperty<string> Value { get; }
    }
}