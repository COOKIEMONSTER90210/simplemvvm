using System;
using Nord.Common.Reactive.Generic;

namespace Samples.Bubbles.ViewModel
{
    public interface IMainFieldViewModel : IDisposable
    {
        IEventProvider<IBubbleViewModel> BubbleAdded { get; }
        IEventProvider<IBubbleViewModel> BubbleRemoved { get; }
    }
}