using System;

namespace Samples.Bubbles.ViewModel
{
    public interface IMainHUDViewModel : IDisposable
    {
        ILabelViewModel Score { get; }
        ILabelViewModel Velocity { get; }
    }
}