﻿namespace Samples.Bubbles.ViewModel
{
    public interface IResultsViewModel
    {
        string ScoreText { get; }

        string VelocityText { get; }

        void StartNewGame();
    }
}