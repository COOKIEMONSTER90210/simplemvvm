using Nord.Common.Reactive.Generic;

namespace Samples.Bubbles.ViewModel
{
    public class LabelViewModel : ILabelViewModel
    {
        private readonly string _description;
        private readonly ReactiveProperty<string> _value = new ReactiveProperty<string>();

        public LabelViewModel(string description, int value)
        {
            _description = description;
            SetValue(value);
        }

        public void SetValue(int value) => _value.Value = $"{_description}: {value}";

        IReactiveProperty<string> ILabelViewModel.Value => _value;
    }
}