using System;
using System.Collections.Generic;
using System.Linq;
using Nord.Common.Reactive.Generic;
using Samples.Bubbles.Logic;

namespace Samples.Bubbles.ViewModel
{
    public class MainFieldViewModel : IMainFieldViewModel
    {
        private readonly GameLogic _logic;
        private readonly List<BubbleViewModel> _spawnedBubbles = new List<BubbleViewModel>();
        private readonly EventProvider<BubbleViewModel> _bubbleAdded = new EventProvider<BubbleViewModel>();
        private readonly EventProvider<BubbleViewModel> _bubbleRemoved = new EventProvider<BubbleViewModel>();
        
        IEventProvider<IBubbleViewModel> IMainFieldViewModel.BubbleAdded => _bubbleAdded;
        IEventProvider<IBubbleViewModel> IMainFieldViewModel.BubbleRemoved => _bubbleRemoved;

        public MainFieldViewModel(GameLogic logic)
        {
            _logic = logic;
            _logic.BubbleAdded += SpawnNewBubble;
            _logic.BubbleRemoved += RemoveBubble;
        }

        void IDisposable.Dispose()
        {
            _logic.BubbleAdded -= SpawnNewBubble;
            _logic.BubbleRemoved -= RemoveBubble;
        }

        private void SpawnNewBubble(IBubble bubble)
        {
            var viewModel = new BubbleViewModel(_logic, bubble);
            _spawnedBubbles.Add(viewModel);
            _bubbleAdded.Call(viewModel);
        }

        private void RemoveBubble(IBubble bubble)
        {
            var candidate = _spawnedBubbles.FirstOrDefault(e => e.GetId() == bubble.GetId());
            if (candidate != null)
            {
                _bubbleRemoved.Call(candidate);
            }
        }
    }
}