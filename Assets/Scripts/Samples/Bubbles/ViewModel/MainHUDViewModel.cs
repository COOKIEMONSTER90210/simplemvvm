using System;
using Samples.Bubbles.Logic;

namespace Samples.Bubbles.ViewModel
{
    public class MainHUDViewModel : IMainHUDViewModel
    {
        private readonly GameLogic _logic;
        private readonly LabelViewModel _score;
        private readonly LabelViewModel _velocity;

        public MainHUDViewModel(GameLogic logic)
        {
            _logic = logic;
            _score = new LabelViewModel("Score", logic.GetScore());
            _velocity = new LabelViewModel("Velocity", logic.GetVelocityForUI());

            _logic.Tick += HandleTick;
            _logic.BubbleRemoved += HandleScoreChanged;
            HandleTick();
        }
        
        void IDisposable.Dispose()
        {
            _logic.Tick -= HandleTick;
            _logic.BubbleRemoved -= HandleScoreChanged;
        }

        private void HandleTick()
        {
            _velocity.SetValue(_logic.GetVelocityForUI());
        }

        private void HandleScoreChanged(IBubble bubble)
        {
            _score.SetValue(_logic.GetScore());
        }

        ILabelViewModel IMainHUDViewModel.Score => _score;
        
        ILabelViewModel IMainHUDViewModel.Velocity => _velocity;
    }
}