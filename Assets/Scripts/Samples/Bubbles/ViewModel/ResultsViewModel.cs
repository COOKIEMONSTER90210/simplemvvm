﻿using Samples.Bubbles.Logic;
using Samples.Bubbles.Router;

namespace Samples.Bubbles.ViewModel
{
    public class ResultsViewModel : IResultsViewModel
    {
        private readonly GameLogic _logic;
        private readonly IMainRouter _router;

        public ResultsViewModel(GameLogic logic, IMainRouter router)
        {
            _logic = logic;
            _router = router;
        }

        string IResultsViewModel.ScoreText => $"Score: {_logic.GetScore()}";

        string IResultsViewModel.VelocityText => $"Velocity: {_logic.GetVelocityForUI()}";

        void IResultsViewModel.StartNewGame() => _router.StartNewGame();
    }
}