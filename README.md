# Nord reactive

This is a simple RX implementation by [Nord studio](https://my.games/studios/2) with source code and an example.
The RX implementation is in the `Nord/Common` folder. The example is in the `Samples/Bubbles folder`.

## Usage

```csharp
// viewModel
public interface ILabelViewModel
{
    IReactiveProperty<string> Value { get; }
}

// view
public class Label : MonoBehaviour
{
    private ILabelViewModel _viewModel;

    public void Init(ILabelViewModel viewModel)
    {
        gameObject.UpdateSimpleViewModel(ref _viewModel, viewModel);
            
        gameObject.Subscribe(viewModel.Value, SetText);
    }

    private void SetText(string value)
    {
        // implementation
    }
}
```
